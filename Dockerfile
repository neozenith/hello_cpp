FROM ubuntu:latest
RUN apt-get update && apt-get install -y \
    build-essential checkinstall cmake git ninja \
    --no-install-recommends && rm -rf /var/lib/apt/lists/*

