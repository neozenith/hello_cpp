cmake_minimum_required(VERSION 3.8)

######################################
# Boost External Project Configuration
######################################
# Download and unpack Boost at configure time
set(BOOST_ROOT "${CMAKE_BINARY_DIR}/boost")

find_package(Boost)
if (NOT Boost_FOUND)
  
  ###################################
  # Configure Variables and Templates
  ###################################
  set(Boost_USE_STATIC_LIBS        ON) # only find static libs
  set(Boost_USE_MULTITHREADED      ON)
  set(Boost_USE_STATIC_RUNTIME    OFF)
  set(BOOSTDL_MAJOR_VERSION 1)
  set(BOOSTDL_MINOR_VERSION 64)
  set(BOOSTDL_FILE "boost_${BOOSTDL_MAJOR_VERSION}_${BOOSTDL_MINOR_VERSION}_0.tar.bz2")
  set(BOOSTDL_URL "https://dl.bintray.com/boostorg/release/${BOOSTDL_MAJOR_VERSION}.${BOOSTDL_MINOR_VERSION}.0/source/")

  # Look for a local file cache first or download from site
  set(BOOSTDL_FILE_CACHE "${CMAKE_SOURCE_DIR}/ext/")
  if (EXISTS "${BOOSTDL_FILE_CACHE}${BOOSTDL_FILE}")
    set(BOOST_SRC_URL "${BOOSTDL_FILE_CACHE}/${BOOSTDL_FILE}")
  else()
    set(BOOST_SRC_URL "${BOOSTDL_URL}/${BOOSTDL_FILE}")
  endif()

  set(BOOST_DOWNLOAD_DIR "${CMAKE_BINARY_DIR}/boost-download")

  set(BOOST_BUILD_COMMAND "${BOOST_ROOT}/b2" -j 12)
  foreach(component IN ITEMS ${BOOST_COMPONENTS})
    list(APPEND BOOST_BUILD_COMMAND "--with-${component}")
  endforeach()

  if (WIN32)
    set(BOOTSTRAP_COMMAND "bootstrap.bat")
  else()
    set(BOOTSTRAP_COMMAND "bootstrap.sh")
  endif()

  # NOTE:
  # BOOST_ROOT has to be different to BOOST_DOWNLOAD_DIR because extracting
  # the archive deletes the target extract location which kills the templated
  # CMakeLists.txt which has the subsequent bootstrap and build commands.
  message("BOOST_SRC_URL = ${BOOST_SRC_URL}")
  message("BOOST_DOWNLOAD_DIR = ${BOOST_DOWNLOAD_DIR}")
  message("BOOST_ROOT = ${BOOST_ROOT}")
  message("BOOST_COMPONENT_ARGS = ${BOOST_COMPONENT_ARGS}")

  # Configure template
  configure_file(
    "${PROJECT_SOURCE_DIR}/cmake/templates/boost-download-CMakeLists.txt.in" 
    "${BOOST_DOWNLOAD_DIR}/CMakeLists.txt"
  )

 

  ##########################
  # Configure download links
  ##########################
  execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" . 
    RESULT_VARIABLE result
    WORKING_DIRECTORY "${BOOST_DOWNLOAD_DIR}"
  )

  if(result)
    message(FATAL_ERROR "CMake step for Boost-Download CONFIGURE failed: ${result}")
  endif()

  ###############################
  # Download, bootstrap and build
  ###############################
  execute_process(COMMAND ${CMAKE_COMMAND} --build . 
    RESULT_VARIABLE result
    WORKING_DIRECTORY "${BOOST_DOWNLOAD_DIR}"
  )

  if(result)
    message(FATAL_ERROR "CMake step for Boost-Download BUILD failed: ${result}")
  endif()
endif()
