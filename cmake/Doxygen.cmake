cmake_minimum_required(VERSION 3.8)

# TODO: Use CMake Options to ask whether developer wants to build documentation
option(BUILD_DOC "Build documentation" ON)

# TODO: Leverage Doxygen dot to update build dependency diagram

# Quick Setup to use Doxygen with CMake
# https://vicrucann.github.io/tutorials/quick-cmake-doxygen/

find_package(Doxygen)
if (DOXYGEN_FOUND)

  set(DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/docs/Doxyfile.in)
  set(DOXYGEN_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)

  configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)
  message("Doxygen build started")

  add_custom_target(doc_doxygen ALL
    COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMENT "Generating API documentation with Doxygen"
    VERBATIM 
  )

else(DOXYGEN_FOUND)
  message("Doxygen was not found. Could not update documentation.")
endif(DOXYGEN_FOUND)

# Generate updated dependency diagram if graphviz output specified
if (DOXYGEN_DOT_FOUND)
  
  if (EXISTS "${CMAKE_BINARY_DIR}/dependency-tree.dot")
    
    execute_process(
      COMMAND 
        ${DOXYGEN_DOT_EXECUTABLE} -x -Tpng 
        "-o${CMAKE_SOURCE_DIR}/docs/dependencies.png" 
        "${CMAKE_BINARY_DIR}/dependency-tree.dot"
      RESULT_VARIABLE result
    )
  
  endif()

else(DOXYGEN_DOT_FOUND)
  message("Doxygen was not found. Could not update documentation.")
endif(DOXYGEN_DOT_FOUND)
