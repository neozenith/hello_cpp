/* Copyright: Copyright 2017 Josh Peak
 * Author: Josh Peak <neozenith.dev@gmail.com>
 * Date: 2017-JAN-13
 * */

#include "greet/Greeting.h" // NOLINT

// Test Framework
#include "gtest/gtest.h"

std::string db_path("fixtures/greetings.db");

TEST(GreetingTest, database_dutch) {
    EXPECT_EQ("Hallo Wereld", Greeting::greet_db("nl", db_path));
}
TEST(GreetingTest, database_not_a_lang_code) {
    EXPECT_EQ("Language code not found", Greeting::greet_db("zzz", db_path));
}
TEST(GreetingTest, database_arabic) {
    EXPECT_EQ("مرحبا بالعالم", Greeting::greet_db("ar", db_path));
}
TEST(GreetingTest, database_hindi) {
    EXPECT_EQ("नमस्ते दुनिया", Greeting::greet_db("hi", db_path));
}
