/* Copyright: Copyright 2017 Josh Peak
 * Author: Josh Peak <neozenith.dev@gmail.com>
 * Date: 2017-JAN-13
 * */

#include "greet/Greeting.h" // NOLINT

// Test Framework
#include "gtest/gtest.h"

TEST(GreetingTest, utf8_cyrillic) {
    EXPECT_EQ("Привет мир", Greeting::greet("ru"));
}
TEST(GreetingTest, utf8_hiragana) {
    EXPECT_EQ("こんにちは世界", Greeting::greet("jp"));
}
