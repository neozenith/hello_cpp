/* Copyright: Copyright 2017 Josh Peak
 * Author: Josh Peak <neozenith.dev@gmail.com>
 * Date: 2017-JAN-13
 * */

#include "greet/Greeting.h" // NOLINT

// Test Framework
#include "gtest/gtest.h"

TEST(GreetingTest, english) {
    EXPECT_EQ("Hello world", Greeting::greet("en"));
}
TEST(GreetingTest, spanish) {
    EXPECT_EQ("Hola Mundo", Greeting::greet("es"));
}
