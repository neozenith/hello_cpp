# Hello C++

[![build status](https://gitlab.com/neozenith/hello_cpp/badges/master/build.svg)](https://gitlab.com/neozenith/hello_cpp/commits/master)

Project to build out template C++ projects and have them validate against 
Googletest and Gitlab CI

# Development Standards

  As at 2017-03-07

 - [CMake 3.1.0+](https://cmake.org/)
 - [Ninja v1.7.2](https://ninja-build.org/)
 - [GraphViz](http://www.graphviz.org/Download..php)
 - [GoogleTest v1.8.0](https://github.com/google/googletest/releases/tag/release-1.8.0) (Unit Testing)
 - [Boost v1.64.0](http://www.boost.org/)
 - [Doxygen](http://www.stack.nl/~dimitri/doxygen/)
 - ISO C++14

 Subject to review annually or as needs or opportunity arises.

# Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Getting Started](#getting-started)
    - [Directory Structure](#directory-structure)
    - [Build Targets and Dependencies](#build-targets-and-dependencies)
    - [Get Build Tools](#get-build-tools)
    - [Clone Repo](#clone-repo)
    - [Build with CMake](#build-with-cmake)
    - [Usage Linux / OSX](#usage-linux-osx)
    - [Usage Windows](#usage-windows)
    - [CI Testing Locally](#ci-testing-locally)
    - [Deployment](#deployment)
- [Targets](#targets)
    - [Hello World](#hello-world)
    - [Hello Library](#hello-library)
    - [Hello CLI](#hello-cli)
    - [Hello Service](#hello-service)
    - [Hello SQL](#hello-sql)
    - [Hello QT GUI](#hello-qt-gui)
    - [Hello OpenGL](#hello-opengl)
    - [Hello Cython](#hello-cython)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

----

# Getting Started


## Build Targets and Dependencies

<img
  src="docs/dependencies.png"
  height=200px
  />

```
cmake --graphviz=build/dependency-tree.dot build
dot -x -Tpng -odocs/dependencies.png build/dependency-tree.dot
```

You will need to install CMake and Ninja Build:

## Get Build Tools
**OSX**

```
brew install cmake ninja -y
```

**Windows**

In a command prompt with admin privileges:

```
choco install cmake --installargs 'ADD_CMAKE_TO_PATH=""System""' -y
choco install ninja -y

:: Reload PATH variable
refreshenv

:: You may need to init Visual Studio variables

:: VS2013
"C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" x64

:: VS2017
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
```

## Clone Repo

```bash 
git clone https://gitlab.com/neozenith/hello_cpp.git
cd hello_cpp
```

## Build with CMake

```bash
cmake -H. -Bbuild -G "Ninja" && cmake --build build
cd build && ctest -VV && cd ..
```


## Usage Linux / OSX

Follow getting started procedures and:

```
./build/helloworld-cli
helloworld v1.3.0.0
Usage:
  -h [ --help ]         This help screen
  -v [ --version ]      Version Information
  -l [ --language ] arg ISO Two letter language code.
  -d [ --database ] arg Path to SQLite3 database with more greetings. Must be
                        used with `--language`.
```

## Usage Windows

Follow getting started procedures and:

```
./Release/HelloWorld.exe
helloworld v1.3.0.0
Usage:
  -h [ --help ]         This help screen
  -v [ --version ]      Version Information
  -l [ --language ] arg ISO Two letter language code.
  -d [ --database ] arg Path to SQLite3 database with more greetings. Must be
                        used with `--language`.
```

## CI Testing Locally

You will need:

1. Gitlab-Runner
  - OSX `brew install gitlab-runner`
  - Windows `choco install gitlab-runner`
2. Run the `shell` executor
  - `gitlab-runner exec shell build_job`

## Deployment

This uses CPack to build the `package` target.

```
cmake --build build --target package --clean-first
```

# Targets

## Hello World

 - Get a basic CPP exe to return the UTF-8 text "Hello world"
 - Pass in a two letter country code and return the localised text of "Hello World"
   - Start with just French, Italian, German, Spanish (aka FIGS)
 - Should use CMake as build system and compile cross platform on:
  - OSX
  - Windows 8.1+ (MSVC 2013+)
  - Linux
    - CentOS 7.2+

## Hello Library
 
 - Same as Hello World, but the functionality should be encapsulated in a library

## Hello CLI

 - Same as above but accepts CLI argument format like [`PyCLI`](http://pythonhosted.org/pyCLI/)
 - It should accept command/subcommand format like [`awscli`](http://docs.aws.amazon.com/cli/latest/reference/)
 - It should allow for command line completion
 - It should support the following commands:
  - `greet [language]` where `language` is ISO two letter language code`
  - `xrate [currency]` where `currency` is ISO three letter currency code
    - Display exchange rate relative to USD.

## Hello Service

 - It should be able to be started as a service cross platform
  - Windows
  - OSX
  - Linux (systemd)
 - It should be support system daemon commands to start/stop/restart/enable/disable
 - It should support system default logging with log rotation
 - It should support the commands of `HelloCLI` as service calls

## Hello SQL
  
  - Acting as `HelloService`, integrate a SQLite Database abstraction
  - It should have a table with a list of supported countries, languages, currencies
  - It should apply database migration changes to schema when service starts
    - Schema V1.0 supports `greet` lookup of FIGS only
    - Schema V1.1 supports `greet` with all offical UN languages
    - Schema V2.0 supports `xrate` initalised from file
    - Schema V2.1 supports `xrate` fetching exchange rates of supported `greet` locales daily

## Hello QT GUI

  - It should have a single form QT GUI
    - It should query the service for list of supported languages
    - It should allow selection of a locale
      - Selection of locale should display greeting and latest exchange rate

## Hello OpenGL

  - It should visualise a line graph of the selected exchange rate

## Hello Cython

 - It should use HelloCLI with PyCLI interface binding to C++ libraries


