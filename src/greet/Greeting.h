
/* Copyright: Copyright 2017 Josh Peak
 * Author: Josh Peak <neozenith.dev@gmail.com>
 * Date: 2017-JAN-13
 * */

#ifndef GREETING_H_
#define GREETING_H_

#include <string>
class Greeting {
  public: // NOLINT
    static std::string greet(std::string language_code);
    static std::string greet_db(std::string language_code, std::string database_path );
};

#endif  // GREETING_H_
