/* Copyright: Copyright 2017 Josh Peak
 * Author: Josh Peak <neozenith.dev@gmail.com>
 * Date: 2017-JAN-13
 * */

// C++
#include <iostream> // NOLINT
#include <sstream>
#include <algorithm>
#include <string>
#include <unordered_map>

#include "Greeting.h" // NOLINT
#include "sqlite3.h"

std::string Greeting::greet(std::string language_code) {
  std::transform(
      language_code.begin(),
      language_code.end(),
      language_code.begin(),
      ::tolower);

  std::unordered_map<std::string, std::string> m;
  m["en"] = "Hello world";
  m["fr"] = "Bonjour le monde";
  m["it"] = "Ciao mondo";
  m["de"] = "Hallo Welt";
  m["es"] = "Hola Mundo";
  m["ru"] = "Привет мир";
  m["jp"] = "こんにちは世界";

  return m[language_code];
}

std::string Greeting::greet_db(std::string language_code, std::string database_path ) {
  std::cout << "SQLite3 version: " << sqlite3_libversion() << std::endl;
  sqlite3 *db;
  sqlite3_stmt* stmt;
  int conn_err = sqlite3_open(database_path.c_str(), &db);
  if (conn_err) {
    fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
    return "Could not open database error";
  }

  std::string sql = "SELECT greeting FROM greetings WHERE lang_code = ? LIMIT 1";

  int prep_status = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, 0);
  if (prep_status != SQLITE_OK)
    return "Could not prepare statement";

  int bind_status = sqlite3_bind_text(stmt, 1, language_code.c_str(), language_code.length(), 0);
  if (bind_status != SQLITE_OK)
    return "Could not bind statement";

  int result = sqlite3_step(stmt);
  if (result != SQLITE_ROW)
    return "Language code not found";

  std::string value((const char*)sqlite3_column_text(stmt,0));

  sqlite3_finalize(stmt);
  sqlite3_close(db);

  return value;
}
