/* Copyright: Copyright 2017 Josh Peak
 * Author: Josh Peak <neozenith.dev@gmail.com>
 * Date: 2017-JAN-13
 * */

// C++
#include <iostream> // NOLINT
#include <sstream>
#include <algorithm>
#include <string>
#include <unordered_map>
// Boost
#include <boost/program_options.hpp>

// Project
#include "config.h" // NOLINT
#include "greet/Greeting.h" // NOLINT

namespace po = boost::program_options;

void version() {
  printf("%s v%s\n",
      PROJECT_NAME,
      PROJECT_VERSION
    );
}

int main(int argc, char** argv) {
  try {
    po::options_description desc{"Usage"};
    desc.add_options()
      ("help,h", "This help screen")
      ("version,v", "Version Information")
      ("language,l", po::value< std::string >(), "ISO Two letter language code.")
      ("database,d", po::value< std::string >(), "Path to SQLite3 database with more greetings. Must be used with `--language`.")
    ;
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("version")) {
      version();
    } else if (vm.count("database") && vm.count("language")) {
      std::cout << Greeting::greet_db(
          vm["language"].as<std::string>(),
          vm["database"].as<std::string>()
          ) << "\n";
    } else if (vm.count("language")) {
      std::cout << Greeting::greet(vm["language"].as<std::string>()) << "\n";
    } else {
      version();
      std::cout << desc << "\n";
    }
  }
  catch(std::exception& e) {
    std::cerr << "Unhandled Exception reached the top of main: "
              << e.what() << ", application will now exit" << std::endl;
  }

  return 0;
}
