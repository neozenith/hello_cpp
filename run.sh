# Configure
BUILD_DIR=build
BUILD_TYPE=Release

BUILD_DIR=build
BUILD_TYPE=Release
GENERATOR="Ninja"
INSTALL_DIR="\./dist"

cmake -H. -B$BUILD_DIR -G $GENERATOR --graphviz=$BUILD_DIR/dependency-tree.dot -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR -DCMAKE_BUILD_TYPE=$BUILD_TYPE

# Build

cmake --build $BUILD_DIR --target all

# Installation Test

cmake --build $BUILD_DIR --target install

# Test

cmake --build $BUILD_DIR --target test
cd $BUILD_DIR 
ctest -VV
cd ..

# Package

cmake --build $BUILD_DIR --target package

# Dependency Graph

# cmake --graphviz=$BUILD_DIR/dependency-tree.dot $BUILD_DIR
dot -x -Tpng -odocs/dependencies.png $BUILD_DIR/dependency-tree.dot
echo "Run the following to see build dependencies:"
echo "open docs/dependencies.png"
