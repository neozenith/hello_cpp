:: Configure

:: Building with Ninja on Windows with VS2013
:: http://stackoverflow.com/questions/31262342/cmake-g-ninja-on-windows-specify-x64
:: choco install ninja -y
:: refreshenv
call "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" x64
:: :: VS2017
:: call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" x64

set BUILD_DIR=build
set BUILD_TYPE=Release
:: set GENERATOR="Visual Studio 12 2013 Win64"
:: set GENERATOR="Visual Studio 15 2017 Win64"
set GENERATOR="Ninja"
set INSTALL_DIR="./dist"

cmake -H. -B%BUILD_DIR% -G %GENERATOR% -Wno-dev -DCMAKE_INSTALL_PREFIX=%INSTALL_DIR%

:: Build

cmake --build %BUILD_DIR% --config %BUILD_TYPE% 

:: Test

cd %BUILD_DIR% 
ctest -VV -C %BUILD_TYPE%
cd ..

:: Package

:: ::WIP:: cmake --build build --target package

:: Dependency Graph

cmake --graphviz=%BUILD_DIR%/dependency-tree.dot %BUILD_DIR%
dot -x -Tpng -odocs/dependencies.png %BUILD_DIR%/dependency-tree.dot
echo "Run the following to see build dependencies:"
echo "open docs/dependencies.png"
